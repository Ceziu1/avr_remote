#include "inc/sd.h"

#include <util/delay.h>
#include "inc/spi.h"

volatile sd_conf_t sd_conf;

static void _command(uint8_t cmd, uint32_t arg, uint8_t crc);
static uint8_t _RES1(void);
static void _RES7(uint8_t *result);
static void _RES3(uint8_t *result);

uint8_t SD_Init(void) {
    *(sd_conf.ddr) |= (1 << sd_conf.cs);
    SPI_Activate(sd_conf.port, sd_conf.cs);
    uint8_t result[5];

    _delay_ms(1);
    
    for(uint8_t i = 10; i > 0; --i)
        SPI_Tx8(0xFF);
    
    //go to idle state
    _command(0x0, 0x0, 0x94);
    result[0] = _RES1();
    if(result[0] != 0x01) return 0x01;
    
    //send interface condition
    _command(0x08, 0x1AA, 0x86);
    _RES7(result);
    if(result[0] != 0x01) return 0x02;
    if(result[4] != 0xAA) return 0x02;
    
    uint8_t init_attempt = 0;
    do {
        if(init_attempt > 100) return 0x03;
        
        //send app command
        _command(0x37, 0x0, 0x0);
        result[0] = _RES1();
        
        //send operation condition
        _command(0x29, 0x0, 0x0);
        result[0] = _RES1();
        
        _delay_ms(10);
        ++init_attempt;
    } while (result[0] != 0x0);
    
    //read OCR
    _command(0x3A, 0x0, 0x0);
    _RES3(result);
    if(!(result[0] & 0x80)) return 0x04;

    SPI_Deactivate(sd_conf.port, sd_conf.cs);
    return 0;
}

uint8_t SD_Read(uint32_t address, uint8_t *buffer, uint8_t skip) {
    SPI_Activate(sd_conf.port, sd_conf.cs);

    _command(0x11, address, 0x0);
    uint8_t result = _RES1();
    if(result != 0xFF) {
        uint32_t read_attempt = 0;
        
        //100 ms delay (SD Spec) => (0.1s * 20000000) / (2 * 8)
        while(++read_attempt <= 125000)
            if((result = SPI_Tx8(0xFF)) != 0xFF) break;
               
        if(result == 0xFE) {
            for(uint16_t i = 0; i < skip; ++i) SPI_Tx8(0xFF);
            for(uint16_t i = 0; i < 256; ++i) *(buffer++) = SPI_Tx8(0xFF);

            SPI_Tx8(0xFF);
            SPI_Tx8(0xFF);
        } else {
            result = 0xFF;
        }
    }
    
    SPI_Deactivate(sd_conf.port, sd_conf.cs);
    return result;
}

static void _command(uint8_t cmd, uint32_t arg, uint8_t crc) {
    SPI_Tx8(cmd | 0x40);
    
    SPI_Tx8((uint8_t) (arg >> 24));
    SPI_Tx8((uint8_t) (arg >> 16));
    SPI_Tx8((uint8_t) (arg >> 8));
    SPI_Tx8((uint8_t) (arg));
    
    SPI_Tx8(crc | 0x01);
}

static uint8_t _RES1(void) {
    uint8_t result;
    
    for (uint8_t i = 8; ((result = SPI_Tx8(0xFF)) == 0xFF) || i == 0; --i);
    
    return result;
}

static void _RES7(uint8_t *result) {
    result[0] = _RES1();
    if(result[0] > 0x01) return;
    
    result[1] = SPI_Tx8(0xFF);
    result[2] = SPI_Tx8(0xFF);
    result[3] = SPI_Tx8(0xFF);
    result[4] = SPI_Tx8(0xFF);
}

static void _RES3(uint8_t *result) {
    _RES7(result);
}

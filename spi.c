#include "inc/spi.h"

void SPI_Init(void) {
    SPI_DDR |= (1 << MOSI) | (1 << SS) | (1 << SCK);
    /*
        SPI Enable
        SPI Master Mode
    */
    SPCR = (1 << SPE) | (1 << MSTR);
    SPSR |= (1 << SPI2X);
}

void SPI_Tx(uint8_t *buff, uint16_t cnt) {
    while(cnt--) {
        SPDR = *(buff++);
        while(!(SPSR & (1<<SPIF)));
    }
}

uint8_t SPI_Tx8(uint8_t data) {
    SPDR = data;
    while(!(SPSR & (1<<SPIF)));
    
    return SPDR;
}

uint16_t SPI_Tx16(uint16_t data) {
    SPDR = (data >> 8) & 0xFF;
    while(!(SPSR & (1<<SPIF)));
    data &= 0xFF; data |= (SPDR << 8);

    SPDR = data & 0xFF;
    while(!(SPSR & (1<<SPIF)));
    data &= 0xFF00; data |= SPDR;
    
    return data;
}
// 
// uint16_t SPI_Tx16(uint16_t data) {
//     union { uint16_t val; struct { uint8_t lsb; uint8_t msb; }; } in, out;
//     in.val = data;
//     if (!(SPCR & _BV(DORD))) {
//       SPDR = in.msb;
//       asm volatile("nop"); // See transfer(uint8_t) function
//       while (!(SPSR & _BV(SPIF))) ;
//       out.msb = SPDR;
//       SPDR = in.lsb;
//       asm volatile("nop");
//       while (!(SPSR & _BV(SPIF))) ;
//       out.lsb = SPDR;
//     } else {
//       SPDR = in.lsb;
//       asm volatile("nop");
//       while (!(SPSR & _BV(SPIF))) ;
//       out.lsb = SPDR;
//       SPDR = in.msb;
//       asm volatile("nop");
//       while (!(SPSR & _BV(SPIF))) ;
//       out.msb = SPDR;
//     }
//     return out.val;
// }

void SPI_Activate(volatile uint8_t *port, uint8_t pin) { *port &= ~(1 << pin); }
void SPI_Deactivate(volatile uint8_t *port, uint8_t pin) { *port |= (1 << pin); }

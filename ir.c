#include "inc/ir.h"

volatile ir_conf_t ir_conf;

// static uint8_t IR_Match(uint16_t measure, uint16_t des);
static uint8_t IR_Match_mark(uint16_t measure, uint16_t des);
static uint8_t IR_Match_space(uint16_t measure, uint16_t des);
// static int16_t IR_RC_level(ir_result_t *res, uint16_t *offset, uint16_t *used, uint16_t t1);

static uint8_t IR_Decode_NEC(ir_result_t *res);
static uint8_t IR_Decode_SONY(ir_result_t *res);
static uint8_t IR_Decode_SAMSUNG(ir_result_t *res);
// static uint8_t IR_Decode_RC5(ir_result_t *res);

void IR_Init(void) {
    cli();

    //Configure timer
    TCCR2A = (1 << WGM21);
    TCCR2B = (1 << CS21);
    OCR2A = 125;//100 for 16MHz
    TCNT2 = 0;

    //Enable timer interrupt
    TIMSK2 = (1 << OCIE2A);
    sei();
    
    ir_conf.state = IDLE;
    *(ir_conf.ddr) &= ~(1 << ir_conf.i_pin);
}

void IR_Reset(void) {
   ir_conf.state = IDLE;
   ir_conf.length = 0;
}

uint8_t IR_Decode(ir_result_t *res) {
    res->buffer = ir_conf.buffer;
    res->length = ir_conf.length;
    res->overflow = ir_conf.overflow;

    if(ir_conf.state != STOP)
        return 0;
    if(IR_Decode_NEC(res))
        return 1;
    if(IR_Decode_SONY(res))
        return 1;
    if(IR_Decode_SAMSUNG(res))
        return 1;
    
//     if(IR_Decode_RC5(res))
//         return 1;
    IR_Reset();
    return 0;
}

static uint8_t IR_Decode_NEC(ir_result_t *res) {
    uint32_t data = 0;
    uint16_t offset = 1;

    if(!IR_Match_mark(res->buffer[offset], IR_NEC_HEADER_MARK))
        return 0;
    ++offset;
    if((ir_conf.length == 4) && IR_Match_space(res->buffer[offset], IR_NEC_REPEAT_SPACE)
            && IR_Match_mark(res->buffer[offset+1], IR_NEC_BIT_MARK)) {
        res->bits = 0;
        res->value = 0xFFFFFFFF;
        res->decode_proto = NEC;
        return 1;
    }
    if(ir_conf.length < (2 * IR_NEC_BITS) + 4)
        return 1;
    if(!IR_Match_space(res->buffer[offset], IR_NEC_HEADER_SPACE))
        return 0;
    ++offset;

    for(uint8_t i= 0; i< IR_NEC_BITS; ++i) {
        if(!IR_Match_mark(res->buffer[offset], IR_NEC_BIT_MARK))
            return 0;
        ++offset;

        if(IR_Match_space(res->buffer[offset], IR_NEC_ONE_SPACE))
            data = (data << 1) | 1;
        else if(IR_Match_space(res->buffer[offset], IR_NEC_ZERO_SPACE))
            data = (data << 1) | 0;
        else
            return 0;
        ++offset;
    }

    res->bits = IR_NEC_BITS;
    res->value = data;
    res->decode_proto = NEC;
    return 1;
}

static uint8_t IR_Decode_SONY(ir_result_t *res) {
    uint32_t data = 0;
    uint16_t offset = 0;
    
    if(ir_conf.length < (2* IR_SONY_BITS)+ 2)
        return 0;
    if(res->buffer[offset]* 50 < IR_SONY_DB_SPACE) {
        res->bits = 0;
        res->value = 0xFFFFFFFF;
        res->decode_proto = SANYO;
        return 1;
    }
    
    ++offset;
    if(!IR_Match_mark(res->buffer[offset], IR_SONY_HEADER_MARK))
        return 0;
    ++offset;
    
    while(offset+ 1 < ir_conf.length) {
        if(!IR_Match_space(res->buffer[offset], IR_SONY_HEADER_SPACE))
            break;
        ++offset;
        
        if(IR_Match_mark(res->buffer[offset], IR_SONY_ONE_MARK))
            data = (data << 1) | 1;
        else if(IR_Match_mark(res->buffer[offset], IR_SONY_ZERO_MARK))
            data = (data << 1) | 0;
        else
            return 0;
        ++offset;
    }
    
    res->bits = (offset- 1)/ 2;
    if(res->bits < 12) {
        res->bits = 0;
        return 0;
    }
    res->value = data;
    res->decode_proto = SONY;
    return 1;
}

static uint8_t IR_Decode_SAMSUNG(ir_result_t *res) {
    uint32_t data = 0;
    uint16_t offset = 1;
    
    if(!IR_Match_mark(res->buffer[offset], IR_SAMSUNG_HEADER_MARK))
        return 0;
    ++offset;
    
    if(ir_conf.length == 4 && IR_Match_space(res->buffer[offset], IR_SAMSUNG_REPEAT_SPACE) && IR_Match_mark(res->buffer[offset+ 1], IR_SAMSUNG_BIT_MARK)) {
        res->bits = 0;
        res->value = 0xFFFFFFFF;
        res->decode_proto = SAMSUNG;
        return 1;
    }
    if(ir_conf.length < (2* IR_SAMSUNG_BITS)+ 4)
        return 0;
    if(!IR_Match_space(res->buffer[offset], IR_SAMSUNG_HEADER_SPACE))
        return 0;
    ++offset;
    
    for(uint8_t i= 0; i< IR_SAMSUNG_BITS; ++i) {
        if(!IR_Match_mark(res->buffer[offset], IR_SAMSUNG_BIT_MARK))
            return 0;
        ++offset;
        
        if(IR_Match_space(res->buffer[offset], IR_SAMSUNG_ONE_SPACE))
            data = (data << 1) | 1;
        else if(IR_Match_mark(res->buffer[offset], IR_SAMSUNG_ZERO_SPACE))
            data = (data << 1) | 0;
        else
            return 0;
        ++offset;
    }
    
    res->bits = IR_SAMSUNG_BITS;
    res->value = data;
    res->decode_proto = SAMSUNG;
    return 1;    
}

// static uint8_t IR_Decode_RC5(ir_result_t *res) {
//     uint16_t nbits = 0;
//     uint32_t data = 0;
//     uint16_t used = 0;
//     uint16_t offset = 1;
//     int16_t levelA, levelB = 0;
// 
//     if(ir_conf.length < IR_RC5_MIN_SAMPLES + 2)
//         return 0;
// 
//     if(IR_RC_level(res, &offset, &used, IR_RC5_T1))
//         return 0;
//     if(IR_RC_level(res, &offset, &used, IR_RC5_T1) != 1)
//         return 0;
//     if(IR_RC_level(res, &offset, &used, IR_RC5_T1))
//         return 0;
// 
//     for (; offset < ir_conf.length; ++nbits) {
//         levelA = IR_RC_level(res, &offset, &used, IR_RC5_T1);
//         levelB = IR_RC_level(res, &offset, &used, IR_RC5_T1);
// 
//         if((levelA == 1) && (levelB == 0))
//             data = (data << 1) | 1;
//         else if((levelA == 0) && (levelB == 1))
//             data = (data << 1) | 0;
//         else
//             return 0;
//     }
// 
//     res->bits = nbits;
//     res->value = data;
//     res->decode_proto = RC5;
//     return 1;
// }

// static uint8_t IR_Match(uint16_t measure, uint16_t des) {
//     return ((measure >= des/67) && (measure <= des/40 + 1));
// }

static uint8_t IR_Match_mark(uint16_t measure, uint16_t des) {
    return ((measure >= (des+100)/67) && (measure <= (des+100)/40 + 1));
}

static uint8_t IR_Match_space(uint16_t measure, uint16_t des) {
    return ((measure >= (des-100)/67) && (measure <= (des-100)/40 + 1));
}

// static int16_t IR_RC_level(ir_result_t *res, uint16_t *offset, uint16_t *used, uint16_t t1) {
//     int16_t width, value, correction, available;
// 
//     if(*offset >= res->length)
//         return 1;
//     width = res->buffer[*offset];
//     value = ((*offset) % 2) ? 0 : 1;
//     correction = (value == 0) ? 100 : -100;
// 
//     if(IR_Match(width, t1 + correction))
//         available = 1;
//     else if(IR_Match(width, 2 * t1 + correction))
//         available = 2;
//     else if(IR_Match(width, 3 * t1 + correction))
//         available = 3;
//     else
//         return -1;
// 
//     ++(*used);
//     if(*used >= available) {
//         *used = 0;
//         ++(*offset);
//     }
//     return value;
// }

ISR(TIMER2_COMPA_vect) {
    uint8_t data = ((*ir_conf.input) & (1 << ir_conf.i_pin));
    data >>= ir_conf.i_pin;
    ++ir_conf.timer;
    if(ir_conf.length >= RAW_BUFFER_LENGTH)
        ir_conf.state = OVF;

    switch(ir_conf.state) {
        case IDLE:
            if(data == 0) {
                if(ir_conf.timer < 100) {
                    ir_conf.timer = 0;
                } else {
                    ir_conf.overflow = 0;
                    ir_conf.length = 0;
                    ir_conf.buffer[ir_conf.length++] = ir_conf.timer;
                    ir_conf.timer = 0;
                    ir_conf.state = MARK;
                }
            }
            break;
        case MARK:
            if(data == 1) {
                ir_conf.buffer[ir_conf.length++] = ir_conf.timer;
                ir_conf.timer = 0;
                ir_conf.state = SPACE;
            }
            break;
        case SPACE:
            if(data == 0) {
                ir_conf.buffer[ir_conf.length++] = ir_conf.timer;
                ir_conf.timer = 0;
                ir_conf.state = MARK;
            } else if(ir_conf.timer > 100) {
                ir_conf.state = STOP;
            }
            break;
        case STOP:
            if(data == 0)
                ir_conf.timer = 0;
            break;
        case OVF:
            ir_conf.overflow = 1;
            ir_conf.state = STOP;
            break;
        default:
            break;
    }
}

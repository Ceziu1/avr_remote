#ifndef IR__H
#define IR__H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "uart.h"

#define RAW_BUFFER_LENGTH       101

//************ NEC ************
#define IR_NEC_BITS             32
#define IR_NEC_HEADER_MARK      9000
#define IR_NEC_HEADER_SPACE     4500
#define IR_NEC_BIT_MARK         560
#define IR_NEC_ONE_SPACE        1690
#define IR_NEC_ZERO_SPACE       560
#define IR_NEC_REPEAT_SPACE     2250

//************ SONY ************
#define IR_SONY_BITS            12
#define IR_SONY_DB_SPACE        500
#define IR_SONY_HEADER_MARK     2400
#define IR_SONY_HEADER_SPACE    600
#define IR_SONY_ONE_MARK        1200
#define IR_SONY_ZERO_MARK       600

//************ SAMSUNG ************
#define IR_SAMSUNG_BITS             32
#define IR_SAMSUNG_HEADER_MARK      4500
#define IR_SAMSUNG_HEADER_SPACE     4500
#define IR_SAMSUNG_ONE_SPACE        1600
#define IR_SAMSUNG_ZERO_SPACE       560
#define IR_SAMSUNG_REPEAT_SPACE     2250
#define IR_SAMSUNG_BIT_MARK         560

//************ RC5 ************
#define IR_RC5_MIN_SAMPLES      11
#define IR_RC5_T1               889

typedef enum {
    IDLE,
    MARK,
    SPACE,
    STOP,
    OVF
} ir_state_t;

typedef enum {
    UNKNOWN = -1,
    NEC,
    SONY,
    SAMSUNG,
    SANYO,
    RC5
} decode_proto_t;

typedef struct {
    volatile uint8_t *ddr;
    volatile uint8_t *input;
    volatile uint8_t *output;

    uint8_t i_pin;
    ir_state_t state;
    uint16_t length;
    uint16_t timer;
    uint16_t buffer[RAW_BUFFER_LENGTH];
    uint8_t overflow;
} ir_conf_t;

typedef struct {
    decode_proto_t decode_proto;
    uint16_t address;
    uint32_t value;
    uint16_t bits;
    volatile uint16_t *buffer;
    uint16_t length;
    uint8_t overflow;
} ir_result_t;

void IR_Init(void);
void IR_Reset(void);
uint8_t IR_Decode(ir_result_t *result);

#endif

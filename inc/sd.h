#ifndef SD__H
#define SD__H

#include <avr/io.h>

typedef struct {
    volatile uint8_t *ddr;
    volatile uint8_t *port;

    uint8_t cs;
} sd_conf_t;


uint8_t SD_Init(void);
uint8_t SD_Read(uint32_t address, uint8_t *buffer, uint8_t skip);

#endif

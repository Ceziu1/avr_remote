#include <avr/eeprom.h>

#include "inc/ili9488.h"
#include "inc/spi.h"

volatile ili9488_conf_t ili9488_conf;

static inline void _command(void) { *(ili9488_conf.port) &= ~(1 << ili9488_conf.dc); }
static inline void _data(void) { *(ili9488_conf.port) |= (1 << ili9488_conf.dc); }
static inline uint8_t _888_to_111(ili9488_color_t *col);
void _get_next_point(uint16_t beg_x, uint16_t beg_y, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, ili9488_color_t *col);
void _set_display_area(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
void _draw_pixel(uint16_t x, uint16_t y, ili9488_color_t *col);
void _draw_circle_eight(uint16_t cx, uint16_t cy, uint16_t x, uint16_t y, ili9488_color_t *col, uint8_t stroke);

void ILI9488_Init(void) {
    uint8_t buff[1];

    *(ili9488_conf.ddr) |= (1 << ili9488_conf.dc) | (1 << ili9488_conf.cs);
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);
    
    _command();

    buff[0] = SOFTRS;
    SPI_Tx(buff, 1);

    _delay_ms(10);

    buff[0] = SLPOUT;
    SPI_Tx(buff, 1);

    _delay_ms(120);

    buff[0] = DISPON;
    SPI_Tx(buff, 1);

    buff[0] = MEMCTL;
    SPI_Tx(buff, 1);
    _data();

    //Portrait mode
    buff[0] = 0x48;
    SPI_Tx(buff, 1);

    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);
}

void ILI9488_Clear_screen(ili9488_color_t *col) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);

    _set_display_area(0, 0, ILI_WIDTH, ILI_HEIGHT);
    uint16_t w= ILI_WIDTH, h= ILI_HEIGHT;
    uint8_t buff[3];

    _command();
    buff[0] = MEMWRT;
    SPI_Tx(buff, 1);

    _data();
    uint16_t i;
    if (ili9488_conf.flags & FAST_MODE) {
        buff[0] = _888_to_111(col);
    } else {
        buff[0] = col->r;
        buff[1] = col->g;
        buff[2] = col->b;
    }

    while(h--) {
        if (ili9488_conf.flags & FAST_MODE) {
            for(i= 0; i< w/ 2; i++) {
                SPI_Tx(buff, 1);
            }
        } else {
            for(i= 0; i< w; i++) {
                SPI_Tx(buff, 3);
            }
        }
    }
    
    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);
}

// void ILI9488_Palette(void) {
//     if (!(ili9488_conf.flags & FAST_MODE))
//         return;
//     uint8_t color = 0x0;
//     uint8_t col = 0;
//     uint8_t row = 0;
//     uint16_t it = 0;
//     uint8_t buff[1];
//     while(color < 64) {
//         _set_display_area(col* 60, row* 40, 60, 40);
// 
//         _command();
//         buff[0] = MEMWRT;
//         SPI_Tx(buff, 1);
// 
//         _data();
//         while(it++ < 60* 40) {
//             buff[0] = color;
//             SPI_Tx(buff, 1);
//         }
//         ++color;
//         it= 0;
//         if ((col+1)* 60 == ILI_WIDTH) {
//             col= 0;
//             ++row;
//         } else ++col;
//     }
// }

void ILI9488_Fast(void) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);
    ili9488_conf.flags |= FAST_MODE;

    uint8_t buff[1];
    _command();
    buff[0] = COLMOD;
    SPI_Tx(buff, 1);
    _data();
    buff[0] = 0x01;
    SPI_Tx(buff, 1);
    
    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);
}

void ILI9488_Slow(void) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);
    ili9488_conf.flags &= ~FAST_MODE;

    uint8_t buff[1];
    _command();
    buff[0] = COLMOD;
    SPI_Tx(buff, 1);
    _data();
    buff[0] = 0x06;
    SPI_Tx(buff, 1);

    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);
}

void ILI9488_Print(uint16_t x, uint16_t y, ili9488_color_t *col, char *s) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);

    uint8_t character, buff[3], i= 0;

    while(*s) {
        for(uint8_t j= 0; j< 8; ++j) {
            character = eeprom_read_byte((uint8_t*)(((*s)- 32)* 8 + j));
            for(uint8_t k= 0; k< 8; ++k) {
                if(!((character >> (7- k)) & 0x01)) continue;
                if (ili9488_conf.flags & PIXEL_SCALE) {
                    _set_display_area(x+ i* 2+ k* 2, y+ j* 2, 1, 1);

                    _command();
                    buff[0] = MEMWRT;
                    SPI_Tx(buff, 1);

                    _data();
                    buff[0] = col->r;
                    buff[1] = col->g;
                    buff[2] = col->b;
                    for(uint8_t s= 0; s< 4; ++s)
                        SPI_Tx(buff, 3);
                } else {
                    _set_display_area(x+ i+ k, y+ j, 0, 0);

                    _command();
                    buff[0] = MEMWRT;
                    SPI_Tx(buff, 1);

                    _data();
                    buff[0] = col->r;
                    buff[1] = col->g;
                    buff[2] = col->b;
                    SPI_Tx(buff, 3);
                }
            }
        }
        i+=8; ++s;
    }

    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);    
}

void ILI9488_Draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, ili9488_color_t *col) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);

    int16_t d, dx, dy, ai, bi, xi, yi;
    int16_t x = x1, y = y1;
    
    if (x1 < x2) {
        xi = 1;
        dx = x2 - x1;
    } else {
        xi = -1;
        dx = x1 - x2;
    }
    if (y1 < y2) {
        yi = 1;
        dy = y2 - y1;
    } else {
        yi = -1;
        dy = y1 - y2;
    }
    
    _draw_pixel(x, y, col);
    
    if (dx > dy) {
        ai = (dy - dx) * 2;
        bi = dy * 2;
        d = bi - dx;
        while (x != x2) {
            if (d >= 0) {
                x += xi;
                y += yi;
                d += ai;
            } else {
                d += bi;
                x += xi;
            }
            _draw_pixel(x, y, col);
        }
     } else {
        ai = ( dx - dy ) * 2;
        bi = dx * 2;
        d = bi - dy;
        while (y != y2) {
            if (d >= 0) {
                x += xi;
                y += yi;
                d += ai;
            } else {
                d += bi;
                y += yi;
            }
            _draw_pixel(x, y, col);
        }
    }

    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);    
}

void ILI9488_Draw_circle(uint16_t cx, uint16_t cy, uint16_t r, ili9488_color_t *col, uint8_t s) {
    SPI_Activate(ili9488_conf.port, ili9488_conf.cs);

    uint16_t x= 0, y= r;
    int16_t d= 3- 2* r;
    
    _draw_circle_eight(cx, cy, x, y, col, s);
    while(y>= x) {
        ++x;
        if(d>= 0) {
            --y;
            d= d+ 4* (x- y)+ 10;
        } else {
            d= d+ 4* x+ 6;
        }
        _draw_circle_eight(cx, cy, x, y, col, s);
    }

    SPI_Deactivate(ili9488_conf.port, ili9488_conf.cs);    
}

void ILI9488_Draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, ili9488_color_t *col, uint8_t s) {
    if(s) {
        while(h--) {
            ILI9488_Draw_line(x, y+ h, x+ w, y+ h, col);
        }
    } else {
        ILI9488_Draw_line(x, y, x+ w, y, col);
        ILI9488_Draw_line(x+ w, y, x+ w, y+ h, col);
        ILI9488_Draw_line(x+ w, y+ h, x, y+ h, col);
        ILI9488_Draw_line(x, y+ h, x, y, col);
    }
}

void ILI9488_Draw_triangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, ili9488_color_t *col, uint8_t s) {
    ILI9488_Draw_line(x1, y1, x2, y2, col);
    ILI9488_Draw_line(x2, y2, x3, y3, col);
    ILI9488_Draw_line(x3, y3, x1, y1, col);
    if(s) {
        _get_next_point(x1, y1, x2, y2, x3, y3, col);
//         while((dest >> 16) != x3 && (dest & 0xFFFF) != y3)//(dest >> 16) != x3 && (dest & 0xFFFF) != y3
//             ILI9488_Draw_line(x1, y1, dest >> 16, dest & 0xFFFF, col);
    }
}

void _get_next_point(uint16_t beg_x, uint16_t beg_y, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, ili9488_color_t *col) {
    int16_t d, dx, dy, ai, bi, xi, yi;
    int16_t x = x1, y = y1;
    
    if (x1 < x2) {
        xi = 1;
        dx = x2 - x1;
    } else {
        xi = -1;
        dx = x1 - x2;
    }
    if (y1 < y2) {
        yi = 1;
        dy = y2 - y1;
    } else {
        yi = -1;
        dy = y1 - y2;
    }
    
    if (dx > dy) {
        ai = (dy - dx) * 2;
        bi = dy * 2;
        d = bi - dx;
        while (x != x2) {
            if (d >= 0) {
                x += xi;
                y += yi;
                d += ai;
            } else {
                d += bi;
                x += xi;
            }
            ILI9488_Draw_line(x, y, beg_x, beg_y, col);
        }
     } else {
        ai = ( dx - dy ) * 2;
        bi = dx * 2;
        d = bi - dy;
        while (y != y2) {
            if (d >= 0) {
                x += xi;
                y += yi;
                d += ai;
            } else {
                d += bi;
                y += yi;
            }
            ILI9488_Draw_line(x, y, beg_x, beg_y, col);
        }
    }
}

void _set_display_area(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
    uint8_t buff[4];
    _command(); buff[0] = ROWAS;
    SPI_Tx(buff, 1);

    _data();
    buff[0] = (y >> 8); buff[1] = (y & 0xFF);
    buff[2] = ((y+w) >> 8); buff[3] = ((y+w) & 0xFF);
    SPI_Tx(buff, 4);

    _command(); buff[0] = COLAS;
    SPI_Tx(buff, 1);

    _data();
    buff[0] = (x >> 8); buff[1] = (x & 0xFF);
    buff[2] = ((x+h) >> 8); buff[3] = ((x+h) & 0xFF);
    SPI_Tx(buff, 4);
}

static inline uint8_t _888_to_111(ili9488_color_t *col) {
    uint8_t r_col = 0;
    r_col |= ((col->b >> 7) << 3) | (col->b >> 7);
    r_col |= ((col->g >> 7) << 4) | ((col->g >> 7) << 1);
    r_col |= ((col->r >> 7) << 5) | ((col->r >> 7) << 2);
    return r_col;
}

void _draw_circle_eight(uint16_t cx, uint16_t cy, uint16_t x, uint16_t y, ili9488_color_t *col, uint8_t s) {
    if(s) {
        ILI9488_Draw_line(cx, cy, cx+ x, cy+ y, col);
        ILI9488_Draw_line(cx, cy, cx- x, cy+ y, col);    
        ILI9488_Draw_line(cx, cy, cx+ x, cy- y, col);
        ILI9488_Draw_line(cx, cy, cx- x, cy- y, col);    
        ILI9488_Draw_line(cx, cy, cx+ y, cy+ x, col);
        ILI9488_Draw_line(cx, cy, cx+ y, cy- x, col);    
        ILI9488_Draw_line(cx, cy, cx- y, cy+ x, col);
        ILI9488_Draw_line(cx, cy, cx- y, cy- x, col);    
    } else {
        _draw_pixel(cx+ x, cy+ y, col);
        _draw_pixel(cx- x, cy+ y, col);
        _draw_pixel(cx+ x, cy- y, col);
        _draw_pixel(cx- x, cy- y, col);
        _draw_pixel(cx+ y, cy+ x, col);
        _draw_pixel(cx- y, cy+ x, col);
        _draw_pixel(cx+ y, cy- x, col);
        _draw_pixel(cx- y, cy- x, col);
    }
}

void _draw_pixel(uint16_t x, uint16_t y, ili9488_color_t *col) {
    uint8_t buff[3];
    _set_display_area(x, y, 0, 0);
    
    _command();
    buff[0] = MEMWRT;
    SPI_Tx(buff, 1);

    _data();
    
    if (ili9488_conf.flags & FAST_MODE) {
        buff[0] = _888_to_111(col);
        SPI_Tx(buff, 1);
    } else {
        buff[0] = col->r;
        buff[1] = col->g;
        buff[2] = col->b;
        SPI_Tx(buff, 3);
    }
}

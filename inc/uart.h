#ifndef UART__H
#define UART__H

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>

#if MCU == __AVR_ATmega168__

#define UART_DDR    DDRD
#define UART_RX     PD0
#define UART_TX     PD1

#endif

#define UART_BUFFER_SIZE    16

void UART_Init(void);
void UART_Transmit(uint8_t* data, uint8_t size);

#endif

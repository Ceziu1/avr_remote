#include <avr/io.h>
// #include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <stdlib.h>

#include "inc/spi.h"
#include "inc/uart.h"
#include "inc/ir.h"
#include "inc/touch.h"
#include "inc/ili9488.h"

extern volatile ir_conf_t ir_conf;
extern volatile ili9488_conf_t ili9488_conf;
extern volatile touch_conf_t touch_conf;
extern volatile uint8_t touch_flag;

enum {
    _NONE       = 0x00,
    _DEBUG      = 0x01,
    _SPI        = 0x02,
    _UART       = 0x04,
    _IR         = 0x08,
    _TFT        = 0x10 | _SPI,
    _TOUCH      = 0x20 | _SPI,
};

void init(uint16_t i);
void samsung_logo_demo(void);

void WDT_Init(void) {
    //disable interrupts
    cli();
    WDTCSR = 24;
    WDTCSR = 33;
    WDTCSR |= (1<<WDE);
    //reset watchdog
//     wdt_reset();
//     set up WDT interrupt
//     WDTCSR = (1<<WDCE)|(1<<WDE);
//     Start watchdog timer with 4s prescaller
//     WDTCSR = (1<<WDE)|(1<<WDP2)|(1<<WDP1)|(1<<WDP0);
    //Enable global interrupts
    sei();
}

void main(void) {

//    Enable pin irq on push button
//    PCICR |= (1<<PCIE1);
//    PCIFR |= (1<<PCIF1);
//    PCMSK1 |= (1<<PCINT12);
//    sei();
//     WDT_Init();
    init(_IR);
    SPI_Activate(touch_conf.port, touch_conf.cs);
    sei();
    
//  TODO: Remove interrupts from critical sections (disable touch when draw or sd read)
//   CHANGE: GRAM not consistent after fast mode
//     ILI9488_Palette();
//     ILI9488_Draw_rect(0, 0, 320, 480, &col, ILI_FILL);


//     for (uint8_t i= 0; i< 20; ++i) {
//         ILI9488_Draw_line((ILI_HEIGHT/ 20)* i, 0, ILI_HEIGHT, (ILI_WIDTH/ 20)* i);
//         ILI9488_Draw_line((ILI_HEIGHT/ 20)* i, ILI_WIDTH, 0, (ILI_WIDTH/ 20)* i);
//     }

    
//     ILI9488_Draw_rect(100, 300, 50, 50, &col, ILI_FILL);
//     ILI9488_Print(50, 150, &col, "STALINOWKAAAA");
//     ILI9488_Draw_circle(150, 350, 50, &col, ILI_FILL);
//     ILI9488_Draw_triangle(120, 200, 100, 250, 300, 110, &col, ILI_FILL);
//     ILI9488_Draw_line(100, 200, 10, 0, &col);


//     uint32_t tmp = 0xaabbccdd;
//     sprintf(arr, "0x%8lx", tmp);
    char arr[16] = {0};
//     UART_Transmit((uint8_t *)"STALIN\n\r", 8);
//     _delay_ms(100);
    touch_point_t point;
//     ir_result_t ir_result;
    for(;;) {
        if(touch_flag) {
            point = TOUCH_Get_coords();
            sprintf(arr, "%d|%d\n\r", point.x, touch_flag);
            UART_Transmit((uint8_t *)arr, 16);
            _delay_ms(250);
        }
//         if(IR_Decode(&ir_result)) {
//             ultoa(ir_result.value, arr, 16);
//             sprintf(arr, "%d\n\r", ir_result.decode_proto);
//             UART_Transmit((uint8_t *)arr, 16);
//             IR_Reset();
//         }
    }
}

void init(uint16_t i) {
    if(i & _DEBUG) {
//    Debug diode 
        DDRC |= (1<<PC5);
        PORTC |= (1<<PC5);
//         _delay_ms(1);
    }
    //***** SPI *****
    if(i & _SPI)
        SPI_Init();
    //***** UART *****
    if(i & _UART)
        UART_Init();
    //***** IR Receiver *****
    if(i & _IR) {
        ir_conf.ddr     = &DDRD;
        ir_conf.output  = &PORTD;
        ir_conf.input   = &PIND;
        ir_conf.i_pin   = PD4;
        ir_conf.state   = IDLE;
        ir_conf.length  = 0;
        
        IR_Init();
    }
    //***** ILI tft screen *****
    if(i & _TFT) {
        ili9488_conf.ddr    = &DDRB;
        ili9488_conf.port   = &PORTB;
        ili9488_conf.dc     = PB2;
        ili9488_conf.cs     = PB1;
        ili9488_conf.flags  = PIXEL_SCALE;

        ILI9488_Init();
        samsung_logo_demo();
    }
    //***** TOUCH screen module *****
    if(i & _TOUCH) {
        touch_conf.ddr      = &DDRD;
        touch_conf.port     = &PORTD;
        touch_conf.irq      = PD2;
        touch_conf.cs       = PD5;
        
        TOUCH_Init();
    }
}

void samsung_logo_demo(void) {
    SPI_Activate(touch_conf.port, touch_conf.cs);
    
//     ili9488_color_t col = {255, 255, 255};
    ili9488_color_t col = {0, 0, 0};
    ILI9488_Fast();
    ILI9488_Clear_screen(&col);
    ILI9488_Slow();

    col.r = 254;
    col.g = 223;
    col.b = 2;
    ILI9488_Draw_triangle(50, 150, 160, 150, 160, 90, &col, ILI_FILL);

    col.r = 238;
    col.g = 182;
    col.b = 185;
    ILI9488_Draw_triangle(270, 150, 160, 150, 160, 90, &col, ILI_FILL);

    col.r = 253;
    col.g = 115;
    col.b = 1;
    ILI9488_Draw_triangle(50, 150, 160, 150, 160, 220, &col, ILI_FILL);
    
    col.r = 242;
    col.g = 37;
    col.b = 126;
    ILI9488_Draw_triangle(270, 150, 160, 150, 160, 220, &col, ILI_FILL);

//     col.r = 25;
//     col.g = 25;
//     col.b = 25;
    col.r = 255;
    col.g = 255;
    col.b = 255;
    ILI9488_Draw_triangle(50, 150, 160, 220, 160, 250, &col, ILI_FILL);
    ILI9488_Draw_triangle(50, 150, 50, 180, 160, 250, &col, ILI_FILL);
    ILI9488_Draw_triangle(50, 180, 50, 320, 80, 340, &col, ILI_FILL);
    ILI9488_Draw_triangle(80, 340, 80, 200, 50, 180, &col, ILI_FILL);

//     col.r = 0;
//     col.g = 0;
//     col.b = 0;
    ILI9488_Draw_triangle(270, 150, 160, 220, 160, 250, &col, ILI_FILL);
    ILI9488_Draw_triangle(270, 150, 270, 180, 160, 250, &col, ILI_FILL);
    ILI9488_Draw_triangle(270, 180, 270, 320, 240, 340, &col, ILI_FILL);
    ILI9488_Draw_triangle(240, 340, 240, 200, 270, 180, &col, ILI_FILL);

    col.r = 231;
    col.g = 232;
    col.b = 98;
    ILI9488_Draw_triangle(160, 250, 80, 200, 80, 300, &col, ILI_FILL);
    
    col.r = 178;
    col.g = 226;
    col.b = 238;
    ILI9488_Draw_triangle(160, 250, 240, 200, 240, 300, &col, ILI_FILL);
    
    col.r = 67;
    col.g = 170;
    col.b = 204;
    ILI9488_Draw_triangle(160, 250, 160, 300, 240, 300, &col, ILI_FILL);
    ILI9488_Draw_rect(160, 300, 80, 20, &col, ILI_FILL);

    col.r = 201;
    col.g = 214;
    col.b = 7;
    ILI9488_Draw_triangle(160, 250, 160, 300, 80, 300, &col, ILI_FILL);
    ILI9488_Draw_rect(80, 300, 80, 20, &col, ILI_FILL);
    
    col.r = 143;
    col.g = 162;
    col.b = 40;
    ILI9488_Draw_rect(80, 320, 80, 20, &col, ILI_FILL);
    ILI9488_Draw_triangle(80, 340, 160, 340, 160, 390, &col, ILI_FILL);

    col.r = 15;
    col.g = 34;
    col.b = 138;
    ILI9488_Draw_rect(160, 320, 80, 20, &col, ILI_FILL);
    ILI9488_Draw_triangle(240, 340, 160, 340, 160, 390, &col, ILI_FILL);
}

// ISR(WDT_vect) {
//     PORTC &= ~(1 << PC5);
// }

/*ISR(PCINT1_vect) {
    if(!(PINC & (1 << PC4))) {
        PORTC ^= (1 << PC5);
       //enable or disable IR reciever 
    }

}*/

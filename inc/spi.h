#ifndef SPI__H
#define SPI__H

#include <avr/io.h>

#if MCU == __AVR_ATmega168__

  #define SPI_DDR   DDRB
  #define SCK       PB5
  #define MISO      PB4
  #define MOSI      PB3
  #define SS        PB2

#endif

void SPI_Init(void);
void SPI_Tx(uint8_t *buff, uint16_t cnt);
uint8_t SPI_Tx8(uint8_t data);
uint16_t SPI_Tx16(uint16_t data);
void SPI_Activate(volatile uint8_t *port, uint8_t pin);
void SPI_Deactivate(volatile uint8_t *port, uint8_t pin);

#endif

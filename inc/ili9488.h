#ifndef ILI9488__H
#define ILI9488__H

#include <avr/io.h>
#include <util/delay.h>

#define FAST_MODE               0x01
#define PIXEL_SCALE             0x02

#define SOFTRS    0x01
#define SLPOUT    0x11
#define COLINV    0x21
#define DISPON    0x29
#define DISPOFF   0x28
#define COLAS     0x2A
#define ROWAS     0x2B
#define MEMWRT    0x2C
#define MEMCTL    0x36
#define COLMOD    0x3A

#define ILI_WIDTH   480
#define ILI_HEIGHT  320

#define ILI_FILL        1
#define ILI_NO_FILL     0

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} ili9488_color_t;

typedef struct {
    volatile uint8_t *ddr;
    volatile uint8_t *port;

    uint8_t dc;
    uint8_t cs;
    uint8_t flags;
} ili9488_conf_t;

void ILI9488_Init(void);
void ILI9488_Clear_screen(ili9488_color_t *color);
void ILI9488_Fast(void);
void ILI9488_Slow(void);
void ILI9488_Print(uint16_t x, uint16_t y, ili9488_color_t *color, char *s);
void ILI9488_Palette(void);
void ILI9488_Draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, ili9488_color_t *color);
void ILI9488_Draw_circle(uint16_t cx, uint16_t cy, uint16_t r, ili9488_color_t *color, uint8_t stroke);
void ILI9488_Draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, ili9488_color_t *color, uint8_t stroke);
void ILI9488_Draw_triangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, ili9488_color_t *color, uint8_t stroke);
#endif

MCU = atmega328p
F_CPU = 16000000ul

DUDE_PROGRAMMER = usbasp
DUDE_PORT = usb

TARGET = main

SRC := $(basename $(wildcard *.c))

OUT := $(SRC:=.o)

# Remote programmer(pc) address and user name
REMOTE = 192.168.1.101
R_USER = pi

OPTIMIZE = Os
CC = avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
OBJSIZE = avr-size

CFLAGS  = -mmcu=$(MCU)
CFLAGS += -$(OPTIMIZE)
CFLAGS += -g
CFLAGS += -std=gnu99
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -ffreestanding
CFLAGS += -fshort-enums
CFLAGS += -fverbose-asm
CFLAGS += -Wall
CFLAGS += -I/usr/avr/include
CFLAGS += -Wstrict-prototypes
CFLAGS += -DF_CPU=$(F_CPU)

ESC = \033[
DEBUG = $(ESC)34;40;1m
PROGRAM = $(ESC)33;40;1m
STOP = $(ESC)0m

all: $(SRC)
	@echo " $(DEBUG)>>> Linking...$(STOP)"
	$(CC) $(CFLAGS) -o output/$(TARGET) $(addprefix output/, $(OUT))
	@echo " $(DEBUG)>>> Creating HEX...$(STOP)"
	$(OBJCOPY) -j .text -j .data -O ihex output/$(TARGET) output/$(TARGET).hex
	@echo "  $(DEBUG)>>> Size dump:$(STOP)"
	$(OBJSIZE) -C --mcu=$(MCU) output/$(TARGET)

clean:
	@echo "$(DEBUG)>>> Removing the rubbish...$(STOP)"
	rm -f output/*

%: %.c
	@echo " $(DEBUG)>>> Creating $@.o file...$(STOP)"
	$(CC) $(CFLAGS) -o output/$@.o -c $<

%.lst: %
	@echo " $(DEBUG)>>> Creating $@ file...$(STOP)"
	$(OBJDUMP) -D $< > $@

program: all install
remote: all send
debug: all output/$(TARGET).lst

send: power
	@echo " $(PROGRAM)>>> Remote Execute...$(STOP)"
	scp output/$(TARGET).hex $(R_USER)@$(REMOTE):/home/pi/Documents
	ssh $(R_USER)@$(REMOTE) 'avrdude -v -p $(MCU) -P $(DUDE_PORT) -c $(DUDE_PROGRAMMER) -U flash:w:Documents/$(TARGET).hex'
	
power:
	@echo " $(PROGRAM)>>> Bringing power up...$(STOP)"
	ssh $(R_USER)@$(REMOTE) sudo Documents/hub-ctrl.c/hub-ctrl -h 0 -P 2 -p 1

shutdown:
	@echo " $(PROGRAM)>>> Shutting down...$(STOP)"
	ssh $(R_USER)@$(REMOTE) sudo Documents/hub-ctrl.c/hub-ctrl -h 0 -P 2 -p 0

install:
	@echo " $(PROGRAM)>>> Programming!$(STOP)"
	avrdude -v -p $(MCU) -P $(DUDE_PORT) -c $(DUDE_PROGRAMMER) -U flash:w:output/$(TARGET).hex	

#include "inc/uart.h"

uint8_t UART_buffer[UART_BUFFER_SIZE] = {0};
uint8_t UART_buffer_cnt = 0;
volatile uint8_t UART_buffer_cur = 0;
volatile uint8_t UART_tx_cplt = 1;

void UART_Init(void) {
    cli();
    UART_DDR = (1 << UART_TX);

    //Fixed boud rate to 115200
//     UBRR0 = 0x0a;//0x08 for 16MHz
    UBRR0 = 0x08;//0x08 for 16MHz
    //Enable transmitter
    UCSR0B = (1 << TXEN0)|(1 << TXCIE0);
    //8 bit transmission 2 stop bits
    UCSR0C = (1 << USBS0)|(3<<UCSZ00);
    sei();
}

void UART_Transmit(uint8_t* data, uint8_t size) {
    if (!UART_tx_cplt)
        return;
    UART_tx_cplt = 0;
    if (size > UART_BUFFER_SIZE)
        UART_buffer_cnt = UART_BUFFER_SIZE;
    else
        UART_buffer_cnt = size;
    memcpy(UART_buffer, data, size);
    UDR0 = UART_buffer[UART_buffer_cur++];
    UCSR0B |= (1<<UDRIE0);
}

ISR(USART_TX_vect) {
    while(UART_buffer_cnt) UART_buffer[UART_buffer_cnt--] = 64;
    UART_buffer[UART_buffer_cnt] = 64;
    UART_buffer_cur = 0;
    UART_tx_cplt = 1;
}

ISR(USART_UDRE_vect) {
    UCSR0B &= ~(1<<UDRIE0);
    if (UART_buffer_cur < UART_buffer_cnt) {
        UDR0 = UART_buffer[UART_buffer_cur++];
        UCSR0B |= (1<<UDRIE0);
    }
}
